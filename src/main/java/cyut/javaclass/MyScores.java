package cyut.javaclass;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public abstract class MyScores implements IAction {
    private final List<Double> doubleList;
    private static final Logger logger = LoggerFactory.getLogger(MyScores.class);

    public MyScores() {
        this.doubleList = doLoadFile();
    }


    /**
     * 載入成績檔（請勿修改此處程式）
     *
     * @return
     */
    private List<Double> loadScores() {
        List<Double> doubleList = new ArrayList<>();
        File file = new File(App.class.getClassLoader().getResource("score.csv").getFile());
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(file))) {
            String line = bufferedReader.readLine();
            while (line != null) {
                logger.info("Score : {}", line);
                doubleList.add(Double.parseDouble(line));
                line = bufferedReader.readLine();
            }
        } catch (IOException e) {
            logger.error(e.getMessage());
        } finally {
            return doubleList;
        }
    }


    /**
     * 請勿修改此處程式
     *
     * @return
     */
    @Override
    public List<Double> doLoadFile() {
        return loadScores();
    }

    /**
     * 取得載入的成績檔
     *
     * @return
     */
    public List<Double> getDoubleList() {
        return doubleList;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        doubleList.stream().forEach((d) -> {
            stringBuilder.append("分數:").append(d).append("\n");
        });
        return stringBuilder.toString();
    }
}
