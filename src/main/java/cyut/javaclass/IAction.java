package cyut.javaclass;

import java.util.List;

public interface IAction {
    /**
     * 載入成績檔
     */
    public List<Double> doLoadFile();

    /**
     * 進行成績計算與顯示
     */
    public String doCacl();
}
