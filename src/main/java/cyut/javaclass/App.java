package cyut.javaclass;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * 期中測驗實作一
 */
public class App extends MyScores {

    private static final Logger logger = LoggerFactory.getLogger(App.class);

    public static void main(String[] args) {
        //此處請勿修改，若修改後有誤請自行負責
        App app = new App();
        app.toString();
        logger.info("{}", app.doCacl());
    }


    /**
     * 建構子，請勿修改程式
     */
    public App() {
        super();
    }


    /**
     * 實作程式的地方
     */
    @Override
    public String doCacl() {
        //此行請勿修改
        List<Double> doubleList = getDoubleList();
        //Todo : 實作由此開始
        int died = 0;//不合理成績數
        double highest = 0;//最高分數
        double lowest = 100;//最低分數
        float total = 0;//總分
        float balance = 0;//平均分數
        int success = 0;//及格
        int out =doubleList.size()-success;
        for(int i=0;i<doubleList.size();i++){
            if(doubleList.get(i) > 100 || doubleList.get(i) < 0){//100為最高分，超過100則為不合理成績
                died++;
            }
            else if (doubleList.get(i) > 59.4){//59.4是最低分
                success++;
                total+=doubleList.get(i);
            }
            else
                total+=doubleList.get(i);

            if (doubleList.get(i)>highest && doubleList.get(i) <= 100){
                highest = doubleList.get(i);
            }
            if (doubleList.get(i)<lowest && doubleList.get(i) >= 0) {
                lowest = doubleList.get(i);
            }
        }
        balance = total / doubleList.size();
        int outter =doubleList.size()-success;

        return "= = 成績分析 = =\n"
                + "考試人數:" + doubleList.size()+"\n"
                +"不合理成績數:" + died +"\n"
                + "最高分數:" + highest + "\n"
                +"最低分數:" + lowest + "\n"
                +"平均分數:" + balance ;//回傳